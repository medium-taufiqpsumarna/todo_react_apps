image: node:alpine3.17

stages:
  - Build
  - Test
  - Deploy

variables:
  SERVER_WEB_PATH: /var/www/apps/

build:
  stage: Build
  script:
    - npm install
    - CI=false npm run build
  artifacts:
    paths:
      - build/

test:
  stage: Test
  script:
      # Install npm, snyk, and snyk-to-html
    - npm install -g npm@latest
    - npm install -g snyk
    - npm install snyk-to-html -g
      # Run snyk help, snyk auth, snyk monitor, snyk test to break build and out report
    - export SNYK_TOKEN=${SNYK_API_TOKEN}
    - snyk auth $SNYK_TOKEN
    - snyk monitor --project-name=$CI_PROJECT_NAME
  #  - snyk test --json | snyk-to-html -o snyk_results_$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA.html
  # Save report to artifacts
  artifacts:
    when: always
    paths: 
  #    - snyk_results_$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA.html
staging-deployment:
  stage: Deploy
  before_script:  
    - 'which ssh-agent || ( apk update && apk add openssh )'
    - mkdir -p ~/.ssh
    - eval $(ssh-agent -s)
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ssh-add <(echo "$SECRET_KEY_base64_STAGING" | base64 -d)
    - scp -r -P ${SSH_PORT_STAGING} build ${SSH_USER_STAGING}@${SSH_HOST_STAGING}:/tmp/
    - |
      ssh -o StrictHostKeyChecking=no $SSH_USER_STAGING@$SSH_HOST_STAGING -p $SSH_PORT_STAGING \
      "sudo mkdir -p ${SERVER_WEB_PATH}; \
      sudo mv /tmp/build/* $SERVER_WEB_PATH; \
      sudo chmod 755 -R $SERVER_WEB_PATH; \
      sudo chown www-data:www-data -R $SERVER_WEB_PATH; \
      sudo systemctl reload nginx; \
      sudo systemctl restart nginx; \
      sudo systemctl status nginx; \
      echo "Cleaning Temporary files!" && rm -rf /tmp/build"
  only:
    refs:
    - staging #Run only for branches

production-deployment:
  stage: Deploy
  before_script:  
    - 'which ssh-agent || ( apk update && apk add openssh )'
    - mkdir -p ~/.ssh
    - eval $(ssh-agent -s)
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ssh-add <(echo "$SECRET_KEY_base64_PRODUCTION" | base64 -d)
    - scp -r -P ${SSH_PORT_PRODUCTION} build ${SSH_USER_PRODUCTION}@${SSH_HOST_PRODUCTION}:/tmp/
    - |
      ssh -o StrictHostKeyChecking=no $SSH_USER_PRODUCTION@$SSH_HOST_PRODUCTION -p $SSH_PORT_PRODUCTION \
      "sudo mkdir -p ${SERVER_WEB_PATH}; \
      sudo mv /tmp/build/* $SERVER_WEB_PATH; \
      sudo chmod 755 -R $SERVER_WEB_PATH; \
      sudo chown www-data:www-data -R $SERVER_WEB_PATH; \
      sudo systemctl reload nginx; \
      sudo systemctl restart nginx; \
      sudo systemctl status nginx; \
      echo "Cleaning Temporary files!" && rm -rf /tmp/build"
  only:
    refs:
    - main #Run only for branches
  when: manual #Run pipeline manually