<h1 align="center">Todo React App  📝</h1>  
<p align="center">
  🖊️ A simple Todo App built using <a href="https://reactjs.org/">React</a> and <a href="https://mantine.dev/">Mantine UI</a>
</p>

## This is a super simple Todo App built using React.js and styled using Mantine UI.

You can view a live demo of the project here: https://javascriptbear.github.io/todo_react_app/
Or, you can watch this video demo

### **(Most likely outdated, its reccomeneded you check out the live demo!)**

https://user-images.githubusercontent.com/109053279/178185429-8de77574-cc2f-429f-8c4e-b1ed9f290894.mp4

## 😃 Features:

- ➕ Add todos
- 🗑️ Delete todos
- ⏸ Save todos to Local Storage when the state changes
- ▶️ Load the todos from Local Storage when the site is loaded again
- 🌙 Light \ dark mode toggle

<p align="center">
  ❤️ Feel free to create issues and contributions for features or bugs to this project.
</p>
<hr>

## ⚙️ CI/CD Workflow & Pipelines
![CI/CD Workflow](./img/CICD_Workflow.png "CI/CD Workflow")
<p align="center">CI/CD Workflow</p>

<br>

![CI/CD Pipeline](./img/CICD_Pipeline.png "CI/CD Pipeline")
<p align="center">CI/CD Pipeline</p>

## 🔁 DevOps Goals
- Create two seperate branches for deployment
  - main used for production environment
  - staging used for staging environment
- Create three Gitlab pipeline stages
  - Build
  - Test
  - Deploy

## 🏗️ Tech Stack Used
- AWS EC2 Instance
- Gitlab
- Nginx
- Cloudflare
- Snyk.io
- Apps (React.js, MantineUI)

# Improvement
1. Use npm ci instead of npm install for faster and deterministic builds in the "build" and "test" stages.
2. Add caching for "node_modules/" directory to speed up builds by reusing dependencies from cache.
3. Use npm audit to generate a JSON report for security vulnerabilities, and then convert it to HTML using snyk-to-html.
4. Use GitLab CI/CD rules to conditionally set variables based on branch names, instead of duplicating jobs for staging and production deployments.
5. Use a single "deploy" job with conditional rules to deploy to either staging or production based on the branch name.
6. Use a separate file ("secret_key") to store the decrypted SSH private key instead of passing it as an environment variable for improved security.

# Footnotes
## 🌲 Repository:
### Github
 - Original Repository https://github.com/javascriptbear/todo_react_app
 - Forked https://github.com/taufiqpsumarna/todo_react_app

### Gitlab
  - https://gitlab.com/medium-taufiqpsumarna/todo_react_apps

## ✍️ Medium Blogs
  - [Mini Project: Gitlab CI/CD Pipeline For React Application | Part1](https://medium.com/@taufiqpsumarna/mini-proyek-gitlab-ci-cd-pipeline-for-react-application-part1-98e3499e77ad)
  - [Mini Project: Gitlab CI/CD Pipeline For React Application | Part2](https://medium.com/@taufiqpsumarna/mini-project-gitlab-ci-cd-pipeline-for-react-application-part2-3ffffbbdb0e9)
  - [Mini Project: Gitlab CI/CD Pipeline For React Application | Part3](https://medium.com/@taufiqpsumarna/mini-project-gitlab-ci-cd-pipeline-for-react-application-part3-f4657bc4de47)
